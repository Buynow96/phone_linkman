#!/usr/bin/env python3
# -*- coding: utf-8 -*-

__author__ = 'Buynow'

from domain.linkman import linkman
import os
from dao.linkman_dao import linkman_dao


# main
def main():
    # welcome
    print('####################')
    print('    欢迎使用电话本   ')
    print('####################')

    #menu
    menu_str = '''
    1、列出所有联系人
    2、按名称查找
    3、添加联系人
    4、删除联系人
    5、导出为文本文档
    6、从文本文件导入
    0、退出
                '''
    opt_code = None;
    while opt_code!='0':
        print(menu_str)
        print('请输入选项：',end='')
        opt_code = input()
        opt_fun(opt_code)

def list_linkman():
    d = dao.list_linkman()
    for (k,v) in d.items():
        print(v)

def add_linkman():
    lm = linkman()
    lm.name = input('请输入联系人名称:')
    lm.phone = input('请输入联系人号码:')
    lm.address = input('请输入联系人地址:')
    dao.add_linkman(lm)


def opt_fun(opt):
    if fun_dict.__contains__(opt):
        fun_dict[opt]()
    elif opt!='0':
        print('无效输入!')


if __name__ == '__main__':
    fun_dict = {
        '1':list_linkman,
        '3':add_linkman,
    }
    # 文本保存路径
    local = os.getcwd() + '\\resources\\data'
    dao = linkman_dao(local)
    main()
    # 备份
    dao.backup_linkman()
